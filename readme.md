## About Weather Sample App

this app is created as a sample implementation to call 3rd party open API and get data and manipulate 

App is using http://openweathermap.org/ weather API to get real-time weather information

## Next step
- make front-end eye candy
- Refactor Laravel dependancies to remove unused modules

## Implementation Approach
 the two framework i had in mind at the beginning  was Laravel and codeignitor. even though 
 codeignitor should be the most suitable framework selection for the current app scope, due to its
 performance and weight, I decided to go with Laravel instead, because of. 
 - built-in dev server (in codeignitor this is only available in v4 which is still in pre-alpha)
 - all the power it packs (routing, artisan, Vuejs etc). 
 - and most of all i simply love coding with it, 
## Prerequisites

- Composer
- npm
- PHP 7.0+


## Installation
- create a database in your mysql server

After cloning this repository,  go to source folder and do the following...

-  Rename .env.example to .env and modify it and provide your database access details there. you need to have an empty database created

   - run the following command/s,
     - composer install
     - composer update
     - npm install
     - npm run dev
     - php artisan key:generate
     - php artisan serve
     
App running on Laravel Development server will be available to access on http://127.0.0.1:8000 

openweather.org API configuration is available in
./config/api.php

## Unit Testing
Unit Test cases are available in ./tests/unit/WeatherControllerTest.php

To execute the Test cases run the following command on source root directory
   - vendor\bin\phpunit tests\unit
   
## Integration Testing
Integration Test cases are available in 
 - ./tests/Integration/WeatherControllerIntegrationTest.php
 - ./tests/Integration/FrontEndIntegrationTest.php

To execute the Test cases run the following command on source root directory
   - vendor\bin\phpunit tests\integration 

## Coding Standards
I have maintained PSR-2 standards while implementing this app.

To execute PHP code sniffer, run this command on app root directory:


- vendor\bin\phpcs --standard=PSR2 app 


## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

VueJS framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
