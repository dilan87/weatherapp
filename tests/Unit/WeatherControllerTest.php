<?php

namespace Tests\Unit;

use App\Http\Controllers\Weather;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Input;
use Mockery;

use Tests\TestCase;


class WeatherControllerTest extends TestCase
{

    private $WeatherController;

    /**
     * Setup test suite
     */
    public function setUp()
    {
        parent::setUp();

        $this->WeatherController = $this->app->make(Weather::class);
    }

    public function tearDown()
    {
        Mockery::close();
    }


    /**
     * Method : getWeather()
     * Test case to see if the method returns an empty array if the request doesn't contain
     * a 'location' GET param
     *
     * @covers Weather::getWeather()
     */
    public function testNoLocationParamReturnsEmptyArrayInGetWeather()
    {
        $guzzleMockClient = $this->setupGuzzleMock(200, []);
        $instance = new Weather($guzzleMockClient);
        $value = $instance->getWeather();
        $this->assertTrue(empty($value));
    }

    /**
     * Method : getWeather()
     * Test case to see the method returns proper json data from API
     *
     * @covers Weather::getWeather()
     */
    public function testValidLocationParamReturnsDataArrayInGetWeather()
    {
        //arrange
        Input::merge(['location' => 'Colombo,LK']);
        $guzzleMockClient = $this->setupGuzzleMock(200, $this->getSampleSuccessAPIResponse());
        $instance = new Weather($guzzleMockClient);

        //act
        $value = $instance->getWeather();

        //assert
        $this->assertJsonStringEqualsJsonString(json_encode($this->getSampleSuccessOutput()), $value->getContent());
    }

    /**
     * Method : getWeather()
     * Test case to see the method returns Not found error message
     *
     * @covers Weather::getWeather()
     */
    public function testValidLocationParamReturnsErrorMessageInGetWeather()
    {
        //arrange
        Input::merge(['location' => 'invalidCity,XX']);
        $guzzleMockClient = $this->setupGuzzleMock(404, $this->getSampleFaildAPIResponse());
        $instance = new Weather($guzzleMockClient);

        //act
        $value = $instance->getWeather();

        //assert
        $this->assertJsonStringEqualsJsonString(json_encode($this->getSampleErrorOutput()), $value->getContent());
    }


    /**
     * Method : convertFahrenheitToCelsius()
     * Test case to see check it returns 0 on invalid input parameter
     *
     * @covers Weather::convertFahrenheitToCelsius()
     */
    public function testConvertFahrenheitToCelsiusMethodWithInvalidInput()
    {
        $value = $this->WeatherController->convertFahrenheitToCelsius('abc#$%^');
        $this->assertEquals(0, $value);
    }

    /**
     * Method : convertFahrenheitToCelsius()
     * Test case to see check it returns proper celsius value on valid input parameter
     *
     * @covers Weather::convertFahrenheitToCelsius()
     */
    public function testConvertFahrenheitToCelsiusMethodWithValidInput()
    {
        $value = $this->WeatherController->convertFahrenheitToCelsius(303.15);
        $this->assertEquals('150.64 °C', $value);
    }


    /**
     * @param int $responseCode
     * @param array $responseData
     * @return Mockery\MockInterface
     *
     */
    private function setupGuzzleMock($responseCode, $responseData)
    {
        $string = json_encode($responseData);
        $response = new Response($responseCode, ['Content-Type' => 'application/json'], $string);

        $guzzle = Mockery::mock(Client::class);
        $guzzle->shouldReceive('request')->andReturn($response);
        return $guzzle;
    }

    /**
     * Sample API response on success
     * @return array
     */
    private function getSampleSuccessAPIResponse()
    {
        return [
            'cod' => 200,
            'main' => [
                'temp' => 43.41,
                'humidity' => 75
            ]
        ];
    }

    /**
     * Sample API response on error
     * @return array
     */
    private function getSampleFaildAPIResponse()
    {
        return [
            'cod' => 404,
            'message' => 'city not found'
        ];
    }


    /**
     * Sample function output on success
     * @return array
     */
    private function getSampleSuccessOutput()
    {
        return [
            'comment' => 'Severely High. even deadly for some asthma related illnesses.',
            'temp' => "6.11 °C",
            'humidity' => 75,
            'cod' => 200,
        ];
    }

    /**
     * Sample function output on error
     * @return array
     */
    private function getSampleErrorOutput()
    {
        return [
            'cod' => 404,
            'message' => 'city not found'
        ];
    }


}
