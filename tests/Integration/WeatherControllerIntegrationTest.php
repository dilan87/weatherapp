<?php

namespace Tests\Integration;

use App\Http\Controllers\Weather;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Input;
use Mockery;

use Illuminate\Foundation\Testing\RefreshDatabase;
use phpDocumentor\Reflection\Types\Integer;
use Tests\TestCase;


class WeatherControllerIntegrationTest extends TestCase
{

    private  $WeatherController;

    /**
     * Setup test suite
     */
    public function setUp()
    {
        parent::setUp();
    }

     /**
     * Method : getWeather()
     * Test case to see the methid returns proper
     */
    public function test_getWeather_method_with_proper_location_param(){
        $response = $this->json('GET', '/api/get-weather', array("location"=>"Colombo,LK"));
        $responseObject = json_decode($response->getContent());
        $this->assertEquals(200, $response->status());
        $this->assertEquals(200,$responseObject->cod);
    }

    /**
     * Method : getWeather()
     * Test case to see if the method returns 404 notfound respond for an unavailable Location parameter
     */
    public function test_getWeather_method_with_unavailable_location_param(){
        $response = $this->json('GET', '/api/get-weather', array("location"=>"Colombo123,LK"));
        $responseObject = json_decode($response->getContent());
        $this->assertEquals(200, $response->status());
        $this->assertEquals(404,$responseObject->cod);
    }

}
