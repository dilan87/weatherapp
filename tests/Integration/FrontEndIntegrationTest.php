<?php

namespace Tests\Integration;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FrontEndTest extends TestCase
{
    /**
     * This test case checks weather home view is retuned when root url is requested
     *
     * @return void
     */
    public function testHomeView()
    {
        $this->get('/')
            ->assertSee('How is the weather in');
    }




}
