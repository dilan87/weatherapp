/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');

window.Vue = require('vue');
import Vue from 'vue';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example', require('./components/Example.vue'),'');

const app = new Vue({
    el: '#app',
    data: {
        searchQuery: null,
        name1: 'Dilan Samarakkody',
        weatherData: null,
        temp: '',
        humidityComment: '',
        humidity: ''
    },
    methods: {
        checkWeather: function () {
            axios.get('api/get-weather', {
                params: {
                    location: this.searchQuery
                }
            }).then((res) => {
                // alert('dilan');
                console.log(res.data.cod);
                if (res.data.cod == 200) {
                    this.humidityComment = 'Humidity : ' + res.data.comment;
                    this.temp = 'Current Temperature : ' + res.data.temp + '';
                    this.humidity = 'Current Humidity : ' + res.data.humidity;
                } else {
                    console.log(res.data);
                    this.humidityComment = res.data.message;
                    this.temp = '';
                    this.humidity = '';
                }

            });
        }
    }
});
