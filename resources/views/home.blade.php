<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Weather App</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="css/app.css" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height" id="app">

    <div class="content col-md-4">
        <div class="title  m-b-md ">
            <div class="form-group">
                <label for="exampleInputEmail1">How is the weather in</label>
                <h1>@{{ searchQuery }}</h1>
                <input v-model="searchQuery" type="email" class="form-control" id="exampleInputEmail1"
                       aria-describedby="emailHelp" placeholder="London,UK">
                <small id="emailHelp" class="form-text text-muted">Enter City and Country code seperated by a comma
                </small>
            </div>
            <button class="btn btn-primary" v-on:click="checkWeather()">Submit</button>
            <div style="border: 1px solid rgba(173,173,173,0.74); padding: 10px; border-radius: 5px; margin-top: 30px;" v-if="humidityComment != ''">
                <h4>@{{ humidityComment }}</h4>
                <p v-if="temp != ''" style="font-weight: bold;"> @{{ temp }}</p>
                <p v-if="temp != ''" style="font-weight: bold;"> @{{ humidity }}</p>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
