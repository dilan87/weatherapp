<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use stdClass;

class Weather extends Controller
{
    public $guzzleRequestClient;

    /**
     * Weather constructor.
     * @param Client $guzzle
     */
    public function __construct(Client $guzzle)
    {
        $this->guzzleRequestClient = $guzzle;
    }

    /**
     * Main functon for getting and forming the weather information
     * @param Input $input
     * @return array
     */
    public function getWeather()
    {
        $location = Input::get('location');
        if ($location != null) {
            $url = config('api.open_weather.base_url') . '?q=' . $location
                . '&appid=' . config('api.open_weather.api_key') . '&units=imperial';
            $weatherData = $this->fetchDataFromApi('GET', $url);
            if ($weatherData->cod == '200') {
                $output = new stdClass();
                $output->comment = $this->getWeatherComment((integer)$weatherData->main->humidity);
                $output->temp = $this->convertFahrenheitToCelsius((integer)$weatherData->main->temp);
                $output->humidity = $weatherData->main->humidity;
                $output->cod = $weatherData->cod;
                return response()->json($output);
            } else {
                return response()->json($weatherData);
            }
        } else {
            return [];
        }
    }

    /**
     * Function to call the 3rd party API to fetch weather data
     * @param $method
     * @param $url
     * @return mixed|\Psr\Http\Message\StreamInterface
     */
    public function fetchDataFromApi($method, $url)
    {

        try {
            $res = $this->guzzleRequestClient->request($method, $url);
            return json_decode($res->getBody()->getContents());
        } catch (ClientException $e) {
            return json_decode($e->getResponse()->getBody()->getContents());
        }
    }

    /**
     * This function converts given fahrenheit value to Celsius
     * @param $fahrenheit
     * @return float|int
     */
    public function convertFahrenheitToCelsius($fahrenheit)
    {

        if (is_numeric($fahrenheit)) {
            return round(($fahrenheit - 32) / 1.8, 2) . ' °C';
        } else {
            return null;
        }
    }

    /**
     * This function generates a comment based on the humidity
     * @param $humidity
     * @return string
     */
    public function getWeatherComment($humidity)
    {
        if ($humidity > 65) {
            $comment = "Severely High. even deadly for some asthma related illnesses.";
        } elseif ($humidity > 62) {
            $comment = "Extremely uncomfortable,fairly oppressive.";
        } elseif ($humidity > 52) {
            $comment = "For somewhat uncomfortable for most people at upper edge.";
        } elseif ($humidity > 46) {
            $comment = "It's OK for most, but all perceive the humidity at upper edge.";
        } elseif ($humidity > 31) {
            $comment = "Awesome!... you have a very comfortable humidity.";
        } else {
            $comment = "hmm... humidity. is bit dry, isn't it?";
        }
        return $comment;
    }
}
